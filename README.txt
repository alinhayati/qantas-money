*************************************************************
APPROACH:

For this challenge, I used MVVM architecture together with RxJava and LiveData. I also used custom
views for the views used in recyclerviews. The repository pattern is also used for separating the UI
from the repository and services.

This implementation provides good separation of concerns and is testable and scalable. This is why
I chose it.

If I had more time I was going to add dependency injection using Dagger 2. I have provided the
structure for this already in the code, so that all the wiring is now setup in the fragments which
can be moved to the ioc container from dagger to provide the objects. The repository layer has the
scope of the app and shall be made singleton in the ioc container. I have not done this in the current
implementation though because of not implementation di.

About the reactive tool used, I have used RxJava2 because the job description mentions RxJava.
I could have done the challenge using kotlin coroutines which I find more readable and efficient.

I have only written one unit test for the repository layer here which addresses one of the main features
of the app, which is sorting by date. I would have added more unit tests, including the error case
and some unit tests for the viewmodels if I put more time.

I have not used any particular library other than the standard ones needed for the challenge.

There are some parts of the code where I have put some comments to describe why I have taken a
particular approach.
