package com.nem.ali.qantasmoneychallenge.repository

import com.nem.ali.qantasmoneychallenge.service.accounts.AccountNetworkService
import com.nem.ali.qantasmoneychallenge.service.transactions.*
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class AccountsTransactionsRepositoryTest {
    private val accountServiceMock: AccountNetworkService =
        Mockito.mock(AccountNetworkService::class.java)
    private val transactionServiceMock: TransactionNetworkService =
        Mockito.mock(TransactionNetworkService::class.java)
    private lateinit var repository: AccountsTransactionsRepository

    private val testAccount =
        AccountsTransactionsRepository.Account(
            "test account id",
            "test account name",
            534.02F,
            352.90F,
            "AUD"
        )
    private val testTransaction1 = TransactionDTO(
        "trans_id1",
        "2019-02-02T12:36:38.019Z",
        "transaction 1",
        300F,
        "AUD",
        MerchantDTO("merchant1", AddressDTO("address1", "Sydney", "Australia", "2000")),
        false
    )
    private val testTransaction2 = TransactionDTO(
        "trans_id2",
        "2019-08-21T09:16:38.018Z",
        "transaction 2",
        400F,
        "AUD",
        MerchantDTO("merchant3", AddressDTO("address3", "Sydney", "Australia", "2000")),
        false
    )
    private val testTransaction3 = TransactionDTO(
        "trans_id3",
        "2019-01-09T22:56:28.011Z",
        "transaction 3",
        500F,
        "AUD",
        MerchantDTO("merchant2", AddressDTO("address2", "Sydney", "Australia", "2000")),
        false
    )

    private val testTransactionsDTO = TransactionsDTO(
        listOf(testTransaction1, testTransaction2, testTransaction3)
    )

    @Before
    fun setup() {
        repository = AccountsTransactionsRepository(accountServiceMock, transactionServiceMock)
    }

    @Test
    fun findListOfTransactionsByAccount_sortsCorrectlyByDateDescendingOrder() {
        Mockito.`when`(transactionServiceMock.findListOfTransactionsByAccountId("test account id"))
            .thenReturn(Single.just(testTransactionsDTO))
        val testObserver = repository.findListOfTransactionsByAccount(testAccount).test()
        Mockito.verify(transactionServiceMock).findListOfTransactionsByAccountId(testAccount.id)
        testObserver.assertValue(
            listOf(
                AccountsTransactionsRepository.TransactionDetails(
                    testAccount,
                    AccountsTransactionsRepository.mapToTransaction(testTransaction2)
                ),
                AccountsTransactionsRepository.TransactionDetails(
                    testAccount,
                    AccountsTransactionsRepository.mapToTransaction(testTransaction1)
                ),
                AccountsTransactionsRepository.TransactionDetails(
                    testAccount,
                    AccountsTransactionsRepository.mapToTransaction(testTransaction3)
                )
            )
        )
    }

    @After
    fun cleanup() {
        Mockito.reset(accountServiceMock,transactionServiceMock)
    }
}

