package com.nem.ali.qantasmoneychallenge.ui.accounts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.nem.ali.qantasmoneychallenge.R
import com.nem.ali.qantasmoneychallenge.extension.getViewModel
import com.nem.ali.qantasmoneychallenge.extension.showShortToast
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.service.BaseNetwork
import com.nem.ali.qantasmoneychallenge.service.accounts.AccountNetworkService
import com.nem.ali.qantasmoneychallenge.service.transactions.TransactionNetworkService
import com.nem.ali.qantasmoneychallenge.ui.BaseFragment
import com.nem.ali.qantasmoneychallenge.ui.ViewModelFactory
import com.nem.ali.qantasmoneychallenge.ui.transactions.TransactionsFragment
import kotlinx.android.synthetic.main.fragment_accounts.*

class AccountsFragment : BaseFragment() {
    private lateinit var viewModelFactory: ViewModelFactory<AccountsViewModel>
    private lateinit var viewModel: AccountsViewModel
    private var accountsAdapter: AccountListAdapter? = null

    companion object {
        fun getInstance(): AccountsFragment {
            return AccountsFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val baseNetwork = BaseNetwork()
        viewModelFactory = ViewModelFactory(
            AccountsViewModel(
                AccountsTransactionsRepository(
                    AccountNetworkService(baseNetwork), TransactionNetworkService(baseNetwork)
                )
            )
        )
        viewModel = getViewModel(context!!, viewModelFactory)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_accounts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        observeViewModel()
        loadPage()
    }

    private fun loadPage() {
        viewModel.loadAccounts()
    }

    private fun observeViewModel() {
        viewModel.accountsListLiveData.observe(this, Observer {
            fragment_accounts_progressBar.visibility = View.GONE
            accountsAdapter?.updateList(it)
        })
        viewModel.accountsListErrorLiveData.observe(this, Observer {
            fragment_accounts_progressBar.visibility = View.GONE
            context?.showShortToast(getString(R.string.error_loading_accounts_list))
        })
    }

    private fun setupAdapter() {
        accountsAdapter = AccountListAdapter().apply {
            accountItemClickObservable.subscribe {
                fragmentManager?.beginTransaction()?.replace(
                    R.id.activity_main_fragment_holder,
                    TransactionsFragment.getInstance(it)
                )?.addToBackStack(this@AccountsFragment.javaClass.simpleName)
                    ?.commitAllowingStateLoss()
            }.autoDispose()
        }
        fragment_accounts_recyclerview.adapter = accountsAdapter
    }
}