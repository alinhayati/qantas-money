package com.nem.ali.qantasmoneychallenge.ui.transactions

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.nem.ali.qantasmoneychallenge.R
import com.nem.ali.qantasmoneychallenge.extension.convertDateMillisToFormattedString
import com.nem.ali.qantasmoneychallenge.ui.BaseConstraintLayoutWithLifecycle
import kotlinx.android.synthetic.main.view_date_row.view.*
import java.text.SimpleDateFormat

private const val dateFormat = "dd MMM yyyy"

class DateItemView(context: Context) : BaseConstraintLayoutWithLifecycle(context) {
    init {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        View.inflate(context, R.layout.view_date_row, this)
    }

    fun bind(date: Long) {
        view_transaction_date.text = date.convertDateMillisToFormattedString(dateFormat)
    }
}