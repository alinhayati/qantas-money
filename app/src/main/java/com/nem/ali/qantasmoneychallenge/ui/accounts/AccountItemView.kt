package com.nem.ali.qantasmoneychallenge.ui.accounts

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.nem.ali.qantasmoneychallenge.R
import com.nem.ali.qantasmoneychallenge.extension.formatToCurrency
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.ui.BaseConstraintLayoutWithLifecycle
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_account_item.view.*

class AccountItemView(context: Context) : BaseConstraintLayoutWithLifecycle(context) {
    private var account: AccountsTransactionsRepository.Account? = null
    private val viewClickSubject = PublishSubject.create<AccountsTransactionsRepository.Account>()
    val viewClickObservable: Observable<AccountsTransactionsRepository.Account> =
        viewClickSubject.hide()

    init {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        View.inflate(context, R.layout.view_account_item, this).apply {
            setOnClickListener { account?.let { viewClickSubject.onNext(it) } }
        }
    }

    fun bind(account: AccountsTransactionsRepository.Account) {
        this.account = account
        val currency=account.currency
        account_item_available_balance.text = account.availableBalance.formatToCurrency(currency)
        account_item_balance.text=account.balance.formatToCurrency(currency)
        account_item_name.text=account.name
    }
}