package com.nem.ali.qantasmoneychallenge.ui.transactions

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.nem.ali.qantasmoneychallenge.R
import com.nem.ali.qantasmoneychallenge.extension.formatToCurrency
import com.nem.ali.qantasmoneychallenge.extension.showPendingIndicator
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.ui.BaseConstraintLayoutWithLifecycle
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_transaction_item.view.*

class TransactionItemView(context: Context) : BaseConstraintLayoutWithLifecycle(context) {
    private var transactionDetails: AccountsTransactionsRepository.TransactionDetails? = null
    private val transactionItemClickSubject =
        PublishSubject.create<AccountsTransactionsRepository.TransactionDetails>()
    val transactionClickObservable:Observable<AccountsTransactionsRepository.TransactionDetails> = transactionItemClickSubject.hide()

    init {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        View.inflate(context, R.layout.view_transaction_item, this).apply {
            setOnClickListener { transactionDetails?.let { transactionItemClickSubject.onNext(it) } }
        }
    }

    fun bind(transactionDetails: AccountsTransactionsRepository.TransactionDetails) {
        this.transactionDetails = transactionDetails
        transactionDetails.apply {
            view_transaction_amount.text =
                this.transaction.amount.formatToCurrency(this.transaction.currency)
            view_transaction_description.text = this.transaction.description
            view_transaction_merchant_name.text = this.transaction.merchant.name
            view_transaction_pending_indicator.showPendingIndicator(this.transaction.amountIsPending)
        }
    }

}