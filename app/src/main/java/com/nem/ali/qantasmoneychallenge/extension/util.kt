package com.nem.ali.qantasmoneychallenge.extension

import android.os.Build
import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.absoluteValue

fun Float.formatToCurrency(currency: String): String {
    return if (this < 0) {
        "-$currency ${this.absoluteValue}"
    } else {
        "$currency ${this.absoluteValue}"
    }
}

fun Long.convertDateMillisToFormattedString(pattern: String): String {
    val simpleDateFormat = SimpleDateFormat(pattern)
    return try {
        simpleDateFormat.format(this)
    } catch (e: ParseException) {
        Log.e(javaClass.simpleName, e.message)
        ""
    }
}

fun String.convertFormattedStringToMillisDate(pattern: String): Long {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(pattern)
        OffsetDateTime.parse(this, formatter).toInstant().toEpochMilli()
    } else {
        val simpleDateFormat = SimpleDateFormat(pattern)
        try {
            val mDate = simpleDateFormat.parse(this)
            mDate.time
        } catch (e: ParseException) {
            Log.e(javaClass.simpleName, e.message)
            0L
        }
    }
}

fun Long.extractDate():String {
    return SimpleDateFormat("HH':'mm").format(Date(this))
}

