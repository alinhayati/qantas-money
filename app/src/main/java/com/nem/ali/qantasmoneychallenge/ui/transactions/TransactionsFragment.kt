package com.nem.ali.qantasmoneychallenge.ui.transactions

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.nem.ali.qantasmoneychallenge.R
import com.nem.ali.qantasmoneychallenge.extension.getViewModel
import com.nem.ali.qantasmoneychallenge.extension.showShortToast
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.service.BaseNetwork
import com.nem.ali.qantasmoneychallenge.service.accounts.AccountNetworkService
import com.nem.ali.qantasmoneychallenge.service.transactions.TransactionNetworkService
import com.nem.ali.qantasmoneychallenge.ui.BaseFragment
import com.nem.ali.qantasmoneychallenge.ui.ViewModelFactory
import com.nem.ali.qantasmoneychallenge.ui.transaction_details.TransactionDetailsActivity
import kotlinx.android.synthetic.main.fragment_transactions.*

class TransactionsFragment private constructor() : BaseFragment() {
    private lateinit var viewModelFactory: ViewModelFactory<TransactionsViewModel>
    private lateinit var viewModel: TransactionsViewModel
    private var transactionsAdapter: TransactionListAdapter? = null
    private var account: AccountsTransactionsRepository.Account? = null

    companion object {
        const val accountsKey = "TRANSACTIONS_KEY"

        fun getInstance(account: AccountsTransactionsRepository.Account): TransactionsFragment {
            return TransactionsFragment().apply {
                val bundle: Bundle = Bundle().apply { putParcelable(accountsKey, account) }
                arguments = bundle
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        account = arguments?.getParcelable(accountsKey)
        val baseNetwork = BaseNetwork()
        viewModelFactory = ViewModelFactory(
            TransactionsViewModel(
                AccountsTransactionsRepository(
                    AccountNetworkService(baseNetwork), TransactionNetworkService(baseNetwork)
                )
            )
        )
        viewModel = getViewModel(context!!, viewModelFactory)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transactions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        observeViewModel()
        loadPage()
    }

    private fun loadPage() {
        account?.let { viewModel.loadTransactions(it) }
    }

    private fun observeViewModel() {
        viewModel.transactionsListLiveData.observe(this, Observer {
            fragment_transactions_progressBar.visibility = View.GONE
            transactionsAdapter?.updateList(it)
        })
        viewModel.transactionsListErrorLiveData.observe(this, Observer {
            fragment_transactions_progressBar.visibility = View.GONE
            context?.showShortToast(getString(R.string.error_loading_transactions_list))
        })
    }

    private fun setupAdapter() {
        transactionsAdapter = TransactionListAdapter().apply {
            transactionItemClickObservable.subscribe {
                val intent = Intent(context, TransactionDetailsActivity::class.java).putExtra(
                    TransactionDetailsActivity.transactionDetailsKey,
                    it
                )
                startActivity(intent)
            }.autoDispose()
        }
        fragment_transactions_recyclerview.adapter = transactionsAdapter
    }
}