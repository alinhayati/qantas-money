package com.nem.ali.qantasmoneychallenge.service.transactions

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nem.ali.qantasmoneychallenge.service.BaseNetwork
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

class TransactionNetworkService(baseNetwork: BaseNetwork) {
    private val transactionNetworkServiceApis =
        baseNetwork.retrofit.create(TransactionNetworkServiceApis::class.java)

    fun findListOfTransactionsByAccountId(accountId: String): Single<TransactionsDTO> {
        // Because the given network api for transaction is not real and does not work with
        // accountId, we use the following dummy accountId to fetch the dummy transaction list
        val dummyAccountId = "7a8a340d-b450-4adc-bbba-f6b4c8cdbc09"
        return transactionNetworkServiceApis.findListOfTransactionsByAccountId(dummyAccountId)
    }
}

interface TransactionNetworkServiceApis {
    @GET("items/{accountId}")
    fun findListOfTransactionsByAccountId(@Path("accountId") accountId: String): Single<TransactionsDTO>
}

@Keep
data class TransactionsDTO(@SerializedName("transactions") val transactions: List<TransactionDTO>)

@Keep
data class TransactionDTO(
    @SerializedName("id") val id: String, @SerializedName("created") val created: String, @SerializedName(
        "description"
    ) val description: String, @SerializedName("amount") val amount: Float, @SerializedName("currency") val currency: String, @SerializedName(
        "merchant"
    ) val merchant: MerchantDTO, @SerializedName("amount_is_pending") val amountIsPending: Boolean
)

@Keep
data class MerchantDTO(@SerializedName("name") val name: String, @SerializedName("address") val address: AddressDTO?)

@Keep
data class AddressDTO(
    @SerializedName("address") val address: String?, @SerializedName("city") val city: String?, @SerializedName(
        "country"
    ) val country: String?, @SerializedName("postcode") val postCode: String?
)