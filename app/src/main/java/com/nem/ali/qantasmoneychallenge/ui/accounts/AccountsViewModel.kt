package com.nem.ali.qantasmoneychallenge.ui.accounts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.ui.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AccountsViewModel(private val accountsTransactionsRepository: AccountsTransactionsRepository) :
    BaseViewModel() {
    private val accountsListMutableLiveData =
        MutableLiveData<List<AccountsTransactionsRepository.Account>>()
    val accountsListLiveData: LiveData<List<AccountsTransactionsRepository.Account>> =
        accountsListMutableLiveData
    private val accountsListErrorMutableLiveData = MutableLiveData<Throwable>()
    val accountsListErrorLiveData: LiveData<Throwable> = accountsListErrorMutableLiveData

    // Here I am passing the exact error received from the repository to the view. In real projects
    // it is better to convert the error object to some actionable error object on which the UI can
    // take action.
    fun loadAccounts() {
        accountsTransactionsRepository.getListOfAccounts().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                accountsListMutableLiveData.value = it
            }, { error ->
                accountsListErrorMutableLiveData.value = error
            }).autoDispose()
    }
}