package com.nem.ali.qantasmoneychallenge.ui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseConstraintLayoutWithLifecycle(context: Context) : ConstraintLayout(context),
    LifecycleObserver {
    private val compositeDisposable = CompositeDisposable()
    protected val customViewLifecycleOwner: LifecycleOwner = when (context) {
        is AppCompatActivity -> {
            context.lifecycle.addObserver(this)
            context
        }

        is Fragment -> {
            context.lifecycle.addObserver(this)
            context
        }
        else -> throw Exception("Unknown context")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected fun onDestroy() {
        compositeDisposable.clear()
    }

    protected fun Disposable.autoDispose() {
        compositeDisposable.add(this)
    }
}