package com.nem.ali.qantasmoneychallenge.ui.transactions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.ui.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TransactionsViewModel(private val accountsTransactionsRepository: AccountsTransactionsRepository) :
    BaseViewModel() {
    private val transactionsListMutableLiveData =
        MutableLiveData<List<AccountsTransactionsRepository.TransactionDetails>>()
    val transactionsListLiveData: LiveData<List<AccountsTransactionsRepository.TransactionDetails>> =
        transactionsListMutableLiveData
    private val transactionsListErrorMutableLiveData = MutableLiveData<Throwable>()
    val transactionsListErrorLiveData: LiveData<Throwable> = transactionsListErrorMutableLiveData

    // Here I am passing the exact error received from the repository to the view. In real projects
    // it is better to convert the error object to some actionable error object on which the UI can
    // take action.
    fun loadTransactions(account: AccountsTransactionsRepository.Account) {
        accountsTransactionsRepository.findListOfTransactionsByAccount(account)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                transactionsListMutableLiveData.value = it
            }, { error ->
                transactionsListErrorMutableLiveData.value = error
            }).autoDispose()
    }
}