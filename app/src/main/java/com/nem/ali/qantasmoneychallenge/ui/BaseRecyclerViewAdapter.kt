package com.nem.ali.qantasmoneychallenge.ui

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

abstract class BaseRecyclerViewAdapter<ItemType>(diffCallback: DiffUtil.ItemCallback<ItemType>) :
    ListAdapter<ItemType, androidx.recyclerview.widget.RecyclerView.ViewHolder>(diffCallback) {

    override fun onDetachedFromRecyclerView(recyclerView: androidx.recyclerview.widget.RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
    }

    abstract class BaseDiffUtilCallback<ItemType> : DiffUtil.ItemCallback<ItemType>() {
        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: ItemType, newItem: ItemType): Boolean {
            return oldItem == newItem
        }
    }
}