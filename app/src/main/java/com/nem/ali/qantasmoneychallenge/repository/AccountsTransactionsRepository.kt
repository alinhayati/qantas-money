package com.nem.ali.qantasmoneychallenge.repository

import android.annotation.SuppressLint
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.nem.ali.qantasmoneychallenge.service.accounts.AccountDTO
import com.nem.ali.qantasmoneychallenge.service.accounts.AccountNetworkService
import com.nem.ali.qantasmoneychallenge.service.transactions.TransactionDTO
import com.nem.ali.qantasmoneychallenge.service.transactions.TransactionNetworkService
import io.reactivex.Observable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

const val datePattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

class AccountsTransactionsRepository(
    private val accountService: AccountNetworkService,
    private val transactionService: TransactionNetworkService
) {
    fun getListOfAccounts(): Observable<List<Account>> {
        // In the following we are passing a dummy user id, which can actually come from the user
        // repository in the real app
        // The reason we are using observable for the list of accounts in the repository is that
        // although the repository here is using only the network service for fetching the list
        // (which emits a single item or error), it can potentially use the local database service
        // for the offline mode in the real app which enables it to listen reactively to the
        // repeated changes in the local database. This can be done using observable.
        return accountService.findListOfAccountsByUserId("some user id")
            .map { it.map { _accountDTO -> _accountDTO.mapToAccount() } }.toObservable()
    }

    fun findListOfTransactionsByAccount(
        account: Account,
        sortOption: SortOption = SortOption.Date
    ): Observable<List<TransactionDetails>> {
        val transactions = transactionService.findListOfTransactionsByAccountId(account.id)
            .map { _transactionsDTO ->
                _transactionsDTO.transactions.map { _transactionDTO ->
                    TransactionDetails(
                        account,
                        mapToTransaction(_transactionDTO)
                    )
                }
            }
        when (sortOption) {
            SortOption.Date -> return transactions.map { it.sortedBy { _transactionDetails -> -_transactionDetails.transaction.dateInMillis } }
                .toObservable()
        }
    }

    enum class SortOption {
        Date
    }

    private fun AccountDTO.mapToAccount(): Account {
        return Account(this.id, this.name, this.balance, this.availableBalance, this.currency)
    }

    companion object {
        fun mapToTransaction(transactionDTO: TransactionDTO): Transaction {
            return with(transactionDTO) {
                Transaction(
                    this.id, this.created, this.description, this.amount, this.currency,
                    Merchant(
                        this.merchant.name,
                        this.merchant.address.let {
                            Address(
                                it?.address,
                                it?.city,
                                it?.country,
                                it?.postCode
                            )
                        }), this.amountIsPending
                )
            }
        }
    }

    data class TransactionDetails(val account: Account, val transaction: Transaction) : Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readParcelable(Account::class.java.classLoader),
            parcel.readParcelable(Transaction::class.java.classLoader)
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeParcelable(account, flags)
            parcel.writeParcelable(transaction, flags)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<TransactionDetails> {
            override fun createFromParcel(parcel: Parcel): TransactionDetails {
                return TransactionDetails(parcel)
            }

            override fun newArray(size: Int): Array<TransactionDetails?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class Account(
        val id: String,
        val name: String,
        val balance: Float,
        val availableBalance: Float,
        val currency: String
    ):Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat(),
            parcel.readFloat(),
            parcel.readString()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(id)
            parcel.writeString(name)
            parcel.writeFloat(balance)
            parcel.writeFloat(availableBalance)
            parcel.writeString(currency)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Account> {
            override fun createFromParcel(parcel: Parcel): Account {
                return Account(parcel)
            }

            override fun newArray(size: Int): Array<Account?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class Transaction(
        val id: String,
        val created: String,
        val description: String,
        val amount: Float,
        val currency: String,
        val merchant: Merchant,
        val amountIsPending: Boolean
    ):Parcelable {
        val dateInMillis: Long
            @SuppressLint("SimpleDateFormat")
            get() {
                val simpleDateFormat = SimpleDateFormat(datePattern)
                return try {
                    val mDate = simpleDateFormat.parse(created)
                    mDate.time
                } catch (e: ParseException) {
                    Log.e(javaClass.simpleName, e.message)
                    0L
                }
            }

        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat(),
            parcel.readString(),
            parcel.readParcelable(Merchant::class.java.classLoader),
            parcel.readByte() != 0.toByte()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(id)
            parcel.writeString(created)
            parcel.writeString(description)
            parcel.writeFloat(amount)
            parcel.writeString(currency)
            parcel.writeParcelable(merchant, flags)
            parcel.writeByte(if (amountIsPending) 1 else 0)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Transaction> {
            override fun createFromParcel(parcel: Parcel): Transaction {
                return Transaction(parcel)
            }

            override fun newArray(size: Int): Array<Transaction?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class Merchant(val name: String, val address: Address):Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readParcelable(Address::class.java.classLoader)
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(name)
            parcel.writeParcelable(address, flags)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Merchant> {
            override fun createFromParcel(parcel: Parcel): Merchant {
                return Merchant(parcel)
            }

            override fun newArray(size: Int): Array<Merchant?> {
                return arrayOfNulls(size)
            }
        }
    }

    data class Address(
        val address: String?,
        val city: String?,
        val country: String?,
        val postCode: String?
    ):Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(address)
            parcel.writeString(city)
            parcel.writeString(country)
            parcel.writeString(postCode)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Address> {
            override fun createFromParcel(parcel: Parcel): Address {
                return Address(parcel)
            }

            override fun newArray(size: Int): Array<Address?> {
                return arrayOfNulls(size)
            }
        }
    }
}