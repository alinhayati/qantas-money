package com.nem.ali.qantasmoneychallenge.ui.transactions

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.ui.BaseRecyclerViewAdapter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat

class TransactionListAdapter :
    BaseRecyclerViewAdapter<TransactionListAdapter.AdapterItem>(
        DiffUtilCallback
    ) {
    private var adapterItems = mutableListOf<AdapterItem>()
    private val transactionItemClickSubject =
        PublishSubject.create<AccountsTransactionsRepository.TransactionDetails>()
    val transactionItemClickObservable: Observable<AccountsTransactionsRepository.TransactionDetails> =
        transactionItemClickSubject.hide()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            AdapterViewType.TimeHeaderViewType.ordinal -> return TransactionsViewHolder(
                DateItemView(
                    parent.context
                )
            )
            AdapterViewType.TransactionItemViewType.ordinal -> return TransactionsViewHolder(
                TransactionItemView(
                    parent.context
                ).apply {
                    transactionClickObservable.subscribe {
                        transactionItemClickSubject.onNext(it)
                    }
                }
            )
            else -> throw Exception("Unsupported view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val adapterItem = adapterItems[position]
        when (adapterItem) {
            is AdapterItem.DateItem -> (holder.itemView as DateItemView).bind(adapterItem.dateInMillis)
            is AdapterItem.TransactionItem -> (holder.itemView as TransactionItemView).bind(
                adapterItem.transactionDetails
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return adapterItems[position].viewType.ordinal
    }

    fun updateList(transactionDetails: List<AccountsTransactionsRepository.TransactionDetails>) {
        val list = mutableListOf<AdapterItem>()
        for (i in transactionDetails.indices) {
            if (i == 0) {
                list.add(
                    AdapterItem.DateItem(
                        transactionDetails[0].transaction.dateInMillis
                    )
                )
                list.add(
                    AdapterItem.TransactionItem(
                        transactionDetails[0]
                    )
                )
                continue
            }
            if (!datesFallOnTheSameDay(
                    transactionDetails[i].transaction.dateInMillis,
                    transactionDetails[i - 1].transaction.dateInMillis
                )
            ) {
                list.add(
                    AdapterItem.DateItem(
                        transactionDetails[i].transaction.dateInMillis
                    )
                )
            }
            list.add(
                AdapterItem.TransactionItem(
                    transactionDetails[i]
                )
            )
        }
        this.adapterItems = list
        submitList(list)
    }

    sealed class AdapterItem(val viewType: AdapterViewType) {
        data class TransactionItem(val transactionDetails: AccountsTransactionsRepository.TransactionDetails) :
            AdapterItem(
                AdapterViewType.TransactionItemViewType
            )

        data class DateItem(val dateInMillis: Long) :
            AdapterItem(
                AdapterViewType.TimeHeaderViewType
            )
    }

    enum class AdapterViewType {
        TimeHeaderViewType, TransactionItemViewType
    }

    class TransactionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private fun datesFallOnTheSameDay(dateInMillis1: Long, dateInMillis2: Long): Boolean {
        val sdf = SimpleDateFormat("yyyyMMdd")
        return sdf.format(dateInMillis1) == sdf.format(dateInMillis2)
    }

    private object DiffUtilCallback : BaseDiffUtilCallback<AdapterItem>() {
        override fun areItemsTheSame(oldItem: AdapterItem, newItem: AdapterItem): Boolean {
            if (oldItem.viewType != newItem.viewType) return false
            if (newItem.viewType == AdapterViewType.TimeHeaderViewType) return false
            return (oldItem as AdapterItem.TransactionItem).transactionDetails.transaction.id == (newItem as AdapterItem.TransactionItem).transactionDetails.transaction.id
        }
    }
}