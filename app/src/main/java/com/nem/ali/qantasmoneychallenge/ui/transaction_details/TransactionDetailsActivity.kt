package com.nem.ali.qantasmoneychallenge.ui.transaction_details

import android.app.Activity
import android.os.Bundle
import com.nem.ali.qantasmoneychallenge.R
import com.nem.ali.qantasmoneychallenge.extension.convertDateMillisToFormattedString
import com.nem.ali.qantasmoneychallenge.extension.extractDate
import com.nem.ali.qantasmoneychallenge.extension.formatToCurrency
import com.nem.ali.qantasmoneychallenge.extension.showPendingIndicator
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import kotlinx.android.synthetic.main.activity_transaction_details.*

class TransactionDetailsActivity : Activity() {
    private var transactionDetails: AccountsTransactionsRepository.TransactionDetails? = null

    companion object {
        const val transactionDetailsKey = "TRANSACTIONS_DETAILS_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        transactionDetails =
            intent?.getParcelableExtra(transactionDetailsKey)
        setContentView(R.layout.activity_transaction_details)
        transactionDetails?.let {
            activity_transaction_pending_indicator.showPendingIndicator(it.transaction.amountIsPending)
            activity_transaction_merchant_name.text = it.transaction.merchant.name
            activity_transaction_date.text =
                it.transaction.dateInMillis.convertDateMillisToFormattedString("dd MMM yyyy")
            activity_transaction_time.text = it.transaction.dateInMillis.extractDate()
            activity_transaction_amount.text =
                it.transaction.amount.formatToCurrency(it.transaction.currency)
            activity_transaction_description.text = it.transaction.description
            activity_transaction_address.text = getString(
                R.string.transaction_address,
                it.transaction.merchant.address.address,
                it.transaction.merchant.address.city
            )
        }
    }
}