package com.nem.ali.qantasmoneychallenge.ui.accounts

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nem.ali.qantasmoneychallenge.repository.AccountsTransactionsRepository
import com.nem.ali.qantasmoneychallenge.ui.BaseRecyclerViewAdapter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class AccountListAdapter :
    BaseRecyclerViewAdapter<AccountListAdapter.AdapterItem>(
        DiffUtilCallback
    ) {
    private var adapterItems = mutableListOf<AdapterItem>()
    private val accountItemClickSubject =
        PublishSubject.create<AccountsTransactionsRepository.Account>()
    val accountItemClickObservable: Observable<AccountsTransactionsRepository.Account> =
        accountItemClickSubject.hide()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            AdapterViewType.AccountItemViewType.ordinal -> return AccountsViewHolder(
                AccountItemView(
                    parent.context
                ).apply {
                    viewClickObservable.subscribe {
                        accountItemClickSubject.onNext(it)
                    }
                }
            )
            else -> throw Exception("Unsupported viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val adapterItem = adapterItems[position]
        when (adapterItem) {
            is AdapterItem.AccountItem -> (holder.itemView as AccountItemView).bind(adapterItem.account)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return adapterItems[position].viewType.ordinal
    }

    fun updateList(accounts: List<AccountsTransactionsRepository.Account>) {
        val list = mutableListOf<AdapterItem>()
        for (account in accounts) {
            list.add(
                AdapterItem.AccountItem(
                    account
                )
            )
        }
        this.adapterItems = list
        submitList(list)
    }

    class AccountsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    sealed class AdapterItem(val viewType: AdapterViewType) {
        data class AccountItem(val account: AccountsTransactionsRepository.Account) :
            AdapterItem(
                AdapterViewType.AccountItemViewType
            )
    }

    enum class AdapterViewType {
        AccountItemViewType
    }

    private object DiffUtilCallback : BaseDiffUtilCallback<AdapterItem>() {
        override fun areItemsTheSame(oldItem: AdapterItem, newItem: AdapterItem): Boolean {
            return (oldItem as AdapterItem.AccountItem).account.id == (newItem as AdapterItem.AccountItem).account.id
        }
    }
}