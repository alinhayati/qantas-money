package com.nem.ali.qantasmoneychallenge

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nem.ali.qantasmoneychallenge.ui.accounts.AccountsFragment

class MainActivity : AppCompatActivity() {
    private val accountsFragment = AccountsFragment.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.activity_main_fragment_holder, accountsFragment)
                .commitAllowingStateLoss()
        }
    }
}