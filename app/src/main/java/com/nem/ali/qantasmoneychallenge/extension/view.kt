package com.nem.ali.qantasmoneychallenge.extension

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.nem.ali.qantasmoneychallenge.R
import kotlinx.android.synthetic.main.view_transaction_item.view.*

fun TextView.enable() {
    isEnabled = true
}

fun ProgressBar.show() {
    visibility = View.VISIBLE
}

fun ProgressBar.hide() {
    visibility = View.GONE
}

inline fun <reified T : ViewModel> getViewModel(
    context: Context,
    vmFactory: ViewModelProvider.Factory
): T {
    return when (context) {
        is Fragment -> ViewModelProviders.of(context, vmFactory)[T::class.java]
        is FragmentActivity -> ViewModelProviders.of(context, vmFactory)[T::class.java]
        else -> throw Exception("Unknown context")
    }
}

fun ImageView.showPendingIndicator(isPending:Boolean) {
    this.setImageResource(if (isPending) R.drawable.ic_pending_24dp else R.drawable.ic_done_black_24dp)
}

fun Context.showShortToast(string: String) {
    Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
}