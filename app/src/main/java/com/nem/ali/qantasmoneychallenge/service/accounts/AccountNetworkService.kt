package com.nem.ali.qantasmoneychallenge.service.accounts

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.nem.ali.qantasmoneychallenge.service.BaseNetwork
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

class AccountNetworkService(baseNetwork: BaseNetwork) {
    private val accountNetworkServiceApis =
        baseNetwork.retrofit.create(AccountNetworkServiceApis::class.java)

    fun findListOfAccountsByUserId(userId: String): Single<List<AccountDTO>> {
        // Because real userId is not known and the provided api is dummy, we use a dummy userId
        // to receive the provided dummy list of transactions
        val dummyUserId = "d97f27b5-caba-4cc8-9f5d-32b0208ec7f0"
        return accountNetworkServiceApis.findListOfAccountsByUserId(dummyUserId).map { it.accounts }
    }

}

interface AccountNetworkServiceApis {
    @GET("items/{userId}")
    fun findListOfAccountsByUserId(@Path("userId") userId: String): Single<AccountsDTO>
}

@Keep
data class AccountsDTO(@SerializedName("accounts") val accounts:List<AccountDTO>)

@Keep
data class AccountDTO(
    @SerializedName("id") val id: String, @SerializedName("name") val name: String, @SerializedName(
        "balance"
    ) val balance: Float, @SerializedName("available_balance") val availableBalance: Float, @SerializedName(
        "currency"
    ) val currency: String
)